var Category = require('../models/Category');
/**
 * GET /admin/category
 * List Category page
 */
exports.getAdminCategory = function(req, res) {
  //if (req.user) return res.redirect('/');
  Category
  .find({})
  .sort({ category: 1})
  .exec(function(err, categories){
    if (err) throw err;

    res.render('category/list-categories', {
      title: 'Category List',
      categories: categories
    });
  });

};
/**
 * GET /admin/category/new
 * Add Category page
 */
exports.getAdminAddCategory = function(req, res) {
  //if (req.user) return res.redirect('/');

  res.render('category/add-category', {
    title: 'Add New Category'
  });

};
/**
 * GET /admin/category/:slug/edit
 * Edit Category page
 */
exports.getAdminEditCategory = function(req, res) {
  //if (req.user) return res.redirect('/');
  Category.findOne({
    'slug': req.params.slug
  }, function(err, category) {
    if (err) throw err;

    res.render('category/edit-category', {
      title: 'Edit Category',
      category: category
    });
  });

};
/**
 * POST /admin/category/update
 * Edit Category page
 */
exports.postAdminEditCategory = function(req, res) {
  //if (req.user) return res.redirect('/');
  req.checkBody('category', 'Category field cannot be empty').notEmpty();
  req.checkBody('slug', 'Category slug field cannot be empty').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    res.redirect('/admin/category'); // TODO edit this stmt to redirect back to the form
  }
    // save to db
    Category.update(
      {"_id": req.body.categoryId},
      {"category": req.body.category, "slug":req.body.slug.toLowerCase()},
      function(err, doc) {
        if (err) {
          throw err;
        } else {
          req.flash('success', "Category updated");
          res.redirect("/admin/category");
        }
      }
    );
};
/**
 * GET /category/:cat/delete
 * Delete Category
 */
exports.getAdminDeleteCategory = function(req, res) {
  //if (req.user) return res.redirect('/');
  Category.remove({
    slug: req.params.slug
  }, function(err) {
    if (err) throw err;

    res.redirect('/category');
  });

};
exports.getAdminDeleteSubcategory = function(req, res) {
  //if (req.user) return res.redirect('/');

  Category.update({
    "slug": req.params.slug
  }, {
    $pop: {
      subcategory: {
        "slug": req.params.subslug
      }
    }
  }, function(err, done) {
    if (err) {
      throw err;
    } else {
      req.flash("success", "Subcategory Deleted!");
      res.redirect('/admin/category/show/' + req.params.slug);
    }
  });

};
/**
 * POST /admin/category
 * Add Category process
 */
exports.postAdminCategory = function(req, res) {
  //if (req.user) return res.redirect('/');
  // validate form and save
  req.assert('category', 'Category field cannot be empty').notEmpty();
  //req.assert('slug', 'Category slug field cannot be empty').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/admin/category');
  }

  // var ss = [];
  // // split the incoming subcategories from  a long string at comma
  // // make the slug from subcat, format this-is-a-subcat (space replaced by dash)
  // var subcats = req.body.subcategory.split(', ');
  // for (var i = 0; i < subcats.length; i++) {
  //   ss.push({
  //     subcategory: subcats[i].trim(),
  //     slug: subcats[i].replace(" ", "-").toLowerCase()
  //   });
  // }

  var newCategory = Category({
    category: req.body.category,
    slug: req.body.slug.toLowerCase(),
    // subcategory: ss
  });


  // save to db
  newCategory.save(function(err) {
    if (err) throw err;
    // else
    req.flash('success', 'Category Saved!');
    res.redirect('/admin/category');
  });

};


exports.getAdminShowCategory = function(req, res) {
  Category.findOne({
    'slug': req.params.slug
  }, function(err, category) {
    if (err) throw err;

    res.render('category/show-category', {
      title: 'Show Category',
      category: category
    });
  });

};

exports.postAdminAddSubcategory = function(req, res) {

  var newSubcategory = {
    "subcategory": req.body.subcategory,
    "slug": req.body.subcategorySlug
  };


  req.checkBody('subcategory', 'Subcategory field cannot be empty').notEmpty();
  req.checkBody('subcategorySlug', 'Subcategory slug field cannot be empty').notEmpty();

  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/admin/category');
  } else {
    //var category = Category.findById(req.body.categoryId);

    Category.update({
      "slug": req.body.categorySlug
    }, {
      $push: {
        "subcategory": newSubcategory
      }
    }, function(err, doc) {
      if (err) {
        throw err;
      } else {
        req.flash('success', "Subcategory added");
        res.redirect("/admin/category/show/" + req.body.categorySlug);
      }
    });
  }
};
