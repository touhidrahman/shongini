var Service = require('../models/Service');
var Category = require('../models/Category');

/**
 * GET /Service
 * List all services to let user choose
 */
exports.getService = function(req, res) {
    Category
    .find({})
    .sort({ category: 1})
    .exec(function(err, categories){
      if (err) throw err;

      res.render('service/select-service', {
        title: 'Select a Category',
        categories: categories
      });
    });
};
/**
 * GET /Service/:category
 * List all services under a certain category
 */
exports.getServiceByCategory = function(req, res) {
  var categorySlug = req.params.category;

  Category
  .find({"slug":categorySlug})
  .exec(function(err, category){
    if (err) throw err;

    Service
    .find({"category":category.category})
    .sort({ packagename: 1})
    .exec(function(err, services){
      if (err) throw err;

      res.render('service/list-services', {
        title: category.category + " Packages",
        services: services,
        subcategories: category
      });
    });
  });
};

exports.jsonService = function(req, res) {

  Service
  .find({})
  .sort({ packagename: 1})
  .exec(function(err, services){
    if (err) throw err;

    res.render('service/list-services', {
      title: 'Services',
      services: services
    });
  });

};

// GET /service/new
// View Add Service Page
exports.getServiceNew = function(req, res) {
  // load categories, for now doing it directly. But should be via ajax and REST

  Category.find({})
  .sort({ category: 1})
  .select({"category" : 1, "subcategory.subcategory" : 1})
  .exec(function(err, categories){
    if (err) throw err;
    res.render('service/add-package', {
      title: 'Add New Service Package',
      categories: categories,
      repop: {  // repopulate fields, initially filled with blanks
        packagename: '',
        qty: '',
        unit: '',
        price: '',
        description: '',
        conditions: '',
        tags: ''
      }
    });
  });

}

// POST /service
// Add Service to DB
exports.postService = function(req, res) {
  // validation
  req.checkBody('categorySubcategory', 'Category field cannot be empty').notEmpty();
  req.checkBody('packagename', 'Package name field cannot be empty').notEmpty();
  req.checkBody('unit', 'Unit field cannot be empty').isAlpha();
  req.checkBody('qty', 'Quantity must be a number').isNumeric();
  req.checkBody('price', 'Price must be a number').isNumeric();

  var errors = req.validationErrors();

  // validation done, now save  process starts
  var ss = req.body.categorySubcategory.split('|');
  var category = ss[0]; // first part is category
  var subcategory = ss[1]; // second part is subcategory

  // split delivery regions
  var deliveryRegion = [];
  var dr = req.body.deliveryRegion.split(',');
  for (var i = 0; i < dr.length; i++) {
    deliveryRegion.push(dr[i].trim());
  }

  // split tags
  var tags = [];
  var tg = req.body.tags.split(',');
  for (var i = 0; i < tg.length; i++) {
    tags.push(tg[i].trim());
  }

  // build the model
  var newService = new Service({
    category: category,
    subcategory: subcategory,
    packagename: req.body.packagename,
    qty: req.body.qty,
    unit: req.body.unit,
    price: req.body.price,
    //albumId: Object.,
    description: req.body.description,
    conditions: req.body.conditions,
    deliveryRegion: deliveryRegion,
    deliveryMethod: req.body.deliveryMethod,
    tags: req.body.tags
  });

  if (errors) {
    req.flash('errors', errors);
    Category.find({})
    .sort({ category: 1})
    .select({"category" : 1, "subcategory.subcategory" : 1})
    .exec(function(err, categories){
      if (err) throw err;
      res.render('service/add-package', {
        title: 'Add New Service Package',
        categories: categories,
        repop: newService
      });
    });
  } else {
    // save to db
    newService.save(function(err, cb) {
      if (err) throw err;
      // else
      req.flash('success', 'New Service Package Saved!');
      res.redirect('/service/show/'+cb._id);
    });
  }
}


exports.getServiceShow = function(req, res)
{
  Service.findById(
    req.params.id,
    function(err, service) {
      res.render('service/show-package', {
        title: service.packagename,
        service: service
      });
    }
  )
}
