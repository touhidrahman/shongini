var mongoose = require('mongoose');

var serviceSchema = new mongoose.Schema({
  category: String,
  subcategory: String,
  packagename: String,
  coverphoto: {type:String, default:"noimage.jpg"},
  qty: Number,
  unit: String,
  price: Number,
  //albumId: Object.,
  description: String,
  conditions: String,
  deliveryRegion: [String],
  deliveryMethod: [String],
  tags: [String]
});

module.exports = mongoose.model('Service', serviceSchema);
