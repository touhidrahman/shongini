var mongoose = require('mongoose');

var categorySchema = new mongoose.Schema({
  slug: { type: String, unique: true, lowercase: true },
  category: String,

  subcategory: [
    {
      subcategory: String,
      slug: String
    }
  ]
});

module.exports = mongoose.model('Category', categorySchema);
